'use strict'
module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define('Device', {
    name: DataTypes.STRING,
    lat: DataTypes.STRING,
    long: DataTypes.STRING,
  }, {})
  Device.associate = function (models) {
    // associations can be defined here
    Device.hasMany(models.Product_Device, {
      foreignKey: 'device_id',
    })
  }
  return Device
}
