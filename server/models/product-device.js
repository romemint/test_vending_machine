'use strict'
module.exports = (sequelize, DataTypes) => {
  const Product_Device = sequelize.define('Product_Device', {
    product_id: DataTypes.INTEGER,
    device_id: DataTypes.INTEGER,
    count: DataTypes.INTEGER
  }, {})
  Product_Device.associate = function (models) {
    // associations can be defined here
    Product_Device.belongsTo(models.Product, {
      foreignKey: 'product_id',
      onDelete: 'CASCADE'
    })
    
    Product_Device.belongsTo(models.Device, {
      foreignKey: 'device_id',
      onDelete: 'CASCADE'
    })
  }
  return Product_Device
}
