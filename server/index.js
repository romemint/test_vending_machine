const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

const config = require('../nuxt.config.js')
const deviceController = require('./controllers/device')
const productController = require('./controllers/product')
const productDeviceController = require('./controllers/product_device')

// Import and Set Nuxt.js options
config.dev = process.env.NODE_ENV !== 'production'

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }
  // JSON Parser
  app.use(express.json())

  // Moji Controller routing
  app.get('/api/device', deviceController.index)
  app.post('/api/device', deviceController.store)
  app.put('/api/device/:id', deviceController.update)
  app.delete('/api/device/:id', deviceController.destroy)

  // Product Controller routing
  app.get('/api/product', productController.index)
  app.post('/api/product', productController.store)
  app.put('/api/product/:id', productController.update)
  app.delete('/api/product/:id', productController.destroy)

  // Product Device Controller routing
  app.get('/api/product-device', productDeviceController.index)
  app.post('/api/product-device', productDeviceController.store)
  app.put('/api/product-device/:id', productDeviceController.update)
  app.put('/api/buy/:id', productDeviceController.buy)
  app.delete('/api/product-device/:id', productDeviceController.destroy)

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
