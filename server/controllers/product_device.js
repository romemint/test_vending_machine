const db = require('../models')
const nodemailer = require('nodemailer');
const config = require('../config/config')
let transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
      user: config.mail.email,
      pass: config.mail.password
  }
});

module.exports = {
  index: async (req, res) => {
    try {
      const productDevices = await db.Product_Device.findAll({
        where: {
          device_id: req.query.id
        }
      })
      return res.json(productDevices)
    } catch (e) {
      return res.status(500).json({ message: 'Cannot get data from database.' })
    }
  },
  store: async (req, res) => {
    const data = req.body
    if (data) {
      try {
        const productDevice = await db.sequelize.transaction((t) => {
          return db.Product_Device.create(data, { transaction: t })
        })
        return res.status(201).json(productDevice)
      } catch (e) {
        return res.status(500).json({ message: 'Cannot store data to database.' })
      }
    }
    return res.status(400).json({ message: 'Bad request.' })
  },
  buy: async (req, res) => {
    const id = req.params.id
    if (id) {
      let data = await db.sequelize.transaction(async(t) => {
        const productDevice = await db.Product_Device.findOne({ where: { id: id } });
        productDevice.count--;
        if(productDevice.count <= config.COUNT_NOTI){
          let mailOptions = {
            from: config.mail.from_email,
            to: config.mail.to_email,
            subject: 'Out of stock',
            text: 'Out of stock '
          };

          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
        }
        return db.Product_Device.update({
          count: productDevice.count
        }, { where: { id } }, { transaction: t })
      })
      return res.json(data)
    }
    return res.status(400).json({ message: 'Bad request.' })
  },
  update: async (req, res) => {
    const id = req.params.id
    const data = req.body
    if (id && data) {
      await db.sequelize.transaction((t) => {
        return db.Product_Device.update(data, { where: { id } }, { transaction: t })
      })
      return res.json(data)
    }
    return res.status(400).json({ message: 'Bad request.' })
  },
  destroy: async (req, res) => {
    const id = req.params.id
    if (id) {
      try {
        await db.Product_Device.destroy({ where: { id } })
        return res.status(204).send()
      } catch (e) {
        return res.status(500).json({ message: 'Cannot remove data from database.' })
      }
    } else {
      return res.status(400).json({ message: 'Bad request.' })
    }
  }
}
