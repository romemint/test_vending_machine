'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product_Device extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Product_Device.belongsTo(models.Product, {
        foreignKey: 'product_id',
        onDelete: 'CASCADE'
      })
      
      Product_Device.belongsTo(models.Device, {
        foreignKey: 'device_id',
        onDelete: 'CASCADE'
      })
    }
  };
  Product_Device.init({
    product_id: DataTypes.INTEGER,
    device_id: DataTypes.INTEGER,
    count: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Product_Device',
  });
  return Product_Device;
};