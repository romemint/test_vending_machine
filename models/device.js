'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Device extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Device.hasMany(models.Product_Device, {
        foreignKey: 'device_id',
      })
    }
  };
  Device.init({
    name: DataTypes.STRING,
    lat: DataTypes.STRING,
    long: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Device',
  });
  return Device;
};